#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Sep 16 01:55:58 2018

@author: danielstein
"""

def extractLocationString(inputstring):
    
    '''
        input: raw transcript as a string
        output:
            outputstring: string of keywords pertaining to location, separated by spaces
            nameString: string of keywords pertaining to person, separated by spaces
            needs: string of needs (medicine, food, or water), separated by commas
        summary:
            uses Google Cloud NLP to analyze word entities as location or person
            if the strings 'medic', 'food', or 'water' are contained in the transcript, the corresponding need is added
        misc:
            requires credientials JSON file 'CallFinder-931284887dc7.json'
    '''
    
    import os
    
    os.environ['GOOGLE_APPLICATION_CREDENTIALS'] = 'CallFinder-931284887dc7.json'
    
    inputstring = ' '.join([word.capitalize() for word in inputstring.split(' ')])
    #print(inputstring)
    
    # Imports the Google Cloud client library
    from google.cloud import language
    from google.cloud.language import enums
    from google.cloud.language import types
    
    def entities_text(text):
        """Detects entities in the text."""
        client = language.LanguageServiceClient()
    
        #if isinstance(text, six.binary_type):
        #    text = text.decode('utf-8')
    
        # Instantiates a plain text document.
        document = types.Document(
            content=text,
            type=enums.Document.Type.PLAIN_TEXT)
    
        # Detects entities in the document. You can also analyze HTML with:
        #   document.type == enums.Document.Type.HTML
        entities = client.analyze_entities(document).entities
    
        # entity types from enums.Entity.Type
        #entity_type = ('UNKNOWN', 'PERSON', 'LOCATION', 'ORGANIZATION',
        #               'EVENT', 'WORK_OF_ART', 'CONSUMER_GOOD', 'OTHER')
        
        #print(entities)
        
        #for entity in entities:
        #    print('=' * 20)
        #    print(u'{:<16}: {}'.format('name', entity.name))
        #    print(u'{:<16}: {}'.format('type', entity_type[entity.type]))
        #    print(u'{:<16}: {}'.format('metadata', entity.metadata))
        #    print(u'{:<16}: {}'.format('salience', entity.salience))
        #    print(u'{:<16}: {}'.format('wikipedia_url',
        #          entity.metadata.get('wikipedia_url', '-')))
        
        return entities
    
    entity_type = ('UNKNOWN', 'PERSON', 'LOCATION', 'ORGANIZATION',
                       'EVENT', 'WORK_OF_ART', 'CONSUMER_GOOD', 'OTHER')
    
    entities = entities_text(inputstring)
    
    locations = []
    nameString = []
    for entity in entities:
        if entity_type[entity.type] == 'LOCATION' or entity_type[entity.type] == 'ORGANIZATION':
            locations.append(entity.name)
        if entity_type[entity.type] == 'PERSON' and entity.name != 'Speaker':
            nameString.append(entity.name)
    
    outputstring = []
    for location in locations:
        #print(location)
        locStart = inputstring.find(location) - 1
        while (locStart >= 0) and (inputstring[locStart] in ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9', ' ']):
            location = inputstring[locStart] + location
            locStart -= 1
        #    print(location)
        #print(location.strip(' '))
        outputstring.append(location.strip(' '))
        
    needs = []
    if 'medic' in inputstring.lower():
        needs.append('medicine')
    if 'food' in inputstring.lower():
        needs.append('food')
    if 'water' in inputstring.lower():
        needs.append('water')
    
    outputstring = ' '.join(outputstring)
    nameString = ' '.join(nameString)
    needs = ', '.join(needs)
    
    print(outputstring, nameString, needs)
    
    return outputstring, nameString, needs


def getLocationCoordinates(location):
    '''
        input: string of keywords pertaining to location, separated by spaces
        output: dictionary containding latitude and longitude
        summary: uses Google Maps Geocodify API to convert address to coordinate
        misc: if no location is found, returns None
    '''
    
    import googlemaps
    
    gmaps = googlemaps.Client(key='AIzaSyCzG0Y-_YV4ctp954P6ZmsV4V1n81w-nmw')
    
    # Geocoding an address
    geocode_result = gmaps.geocode(location)
    
    try:
        #print(json.dumps(geocode_result, indent=2))
        return (geocode_result[0]['geometry']['location'])
    except:
        return None