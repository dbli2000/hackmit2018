import os
os.system('export GOOGLE_APPLICATION_CREDENTIALS="/Users/danielstein/Downloads/CallFinder-931284887dc7.json"')

inputstring = "the event is on mit's campus"
inputstring = ' '.join([word.capitalize() for word in inputstring.split(' ')])
print(inputstring)

# Imports the Google Cloud client library
from google.cloud import language
from google.cloud.language import enums
from google.cloud.language import types

def entities_text(text):
    """Detects entities in the text."""
    client = language.LanguageServiceClient()

    #if isinstance(text, six.binary_type):
    #    text = text.decode('utf-8')

    # Instantiates a plain text document.
    document = types.Document(
        content=text,
        type=enums.Document.Type.PLAIN_TEXT)

    # Detects entities in the document. You can also analyze HTML with:
    #   document.type == enums.Document.Type.HTML
    entities = client.analyze_entities(document).entities

    # entity types from enums.Entity.Type
    entity_type = ('UNKNOWN', 'PERSON', 'LOCATION', 'ORGANIZATION',
                   'EVENT', 'WORK_OF_ART', 'CONSUMER_GOOD', 'OTHER')

    for entity in entities:
        print('=' * 20)
        print(u'{:<16}: {}'.format('name', entity.name))
        print(u'{:<16}: {}'.format('type', entity_type[entity.type]))
        print(u'{:<16}: {}'.format('metadata', entity.metadata))
        print(u'{:<16}: {}'.format('salience', entity.salience))
        print(u'{:<16}: {}'.format('wikipedia_url',
              entity.metadata.get('wikipedia_url', '-')))
    
    return entities

entity_type = ('UNKNOWN', 'PERSON', 'LOCATION', 'ORGANIZATION',
                   'EVENT', 'WORK_OF_ART', 'CONSUMER_GOOD', 'OTHER')

entities = entities_text(inputstring)

locations = []
for entity in entities:
    if entity_type[entity.type] == 'LOCATION' or entity_type[entity.type] == 'ORGANIZATION':
        locations.append(entity.name)

outputstring = []

for location in locations:
    #print(location)
    locStart = inputstring.find(location) - 1
    while (locStart >= 0) and (inputstring[locStart] in ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9', ' ']):
        location = inputstring[locStart] + location
        locStart -= 1
    #    print(location)
    #print(location.strip(' '))
    outputstring.append(location.strip(' '))

outputstring = ' '.join(outputstring)

print(outputstring)