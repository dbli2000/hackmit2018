#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Sep 16 02:01:12 2018

@author: danielstein
"""

def writeHTML(coords, names, needs):
    '''
        input:
            coords: list of latitudes and longitudes as dictionaries
            names: list of names as strings
            needs: list of needs as strings
        output:
            returns string of HTML file
        summary:
            reads in a template file EmptyMap.html to build a Google Map HTML file,
            modifies three lines, and writes out a new HTML file Output.html
        misc:
            requires timplate file 'EmptyMap.html' in the same directory,
            writes out Output.html to the same directory
    '''
    
    with open('EmptyMap.html', 'r') as f:
        template = f.read()
    
    template = template.split('\n')
    template[28] = template[28][:21] + str(coords) + ';'
    template[29] = template[29][:21] + str(names) + ';'
    template[30] = template[30][:21] + str(needs) + ';'
    template = '\n'.join(template)
    
    with open('Output.html', 'w') as f:
        f.write(template)
        
    return template