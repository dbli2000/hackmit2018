# -*- coding: utf-8 -*-
"""
Created on Sat Sep 15 14:46:57 2018

@author: dbli2
"""
#Import  required modules
from Transcribe import Transcribe
from  RecoverText import RecoverText

def RevSpeech(key,  filename):
    '''
    Inputs: 
        key: Authentication Key for RevSpeech
        filename: Name of .mp3 file
        #mp4URL: URL for .mp3 file to be transcribed
        
    Outputs: 
        Returns string of the words spoken in the .mp3 file
        
    Summary:
        Takes a .mp4 and uses revspeech to try to convert it to a text file
        Prints status of upload and ID Number of  file uploaded
        Returns text file
   ''' 
    #Submits .mp4 to RevSpeech and recovers ID of submission
    ID  = Transcribe(key, filename)   

    #Recovers text file
    ReturnedText = RecoverText(key, ID)

    #Returns string of conversation. 
    return ReturnedText
