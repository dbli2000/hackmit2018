# -*- coding: utf-8 -*-
"""
Created on Sat Sep 15 14:46:57 2018

@author: dbli2
"""
#Import  required modules
import os
import ast

def Transcribe(Key, FileName):
   '''
    Inputs: 
        key: Authentication Key for RevSpeech
        mp4URL: URL for .mp4 file to be transcribed
        
    Outputs: 
        Returns ID number of  RevSpeech instance processing the file
        
    Summary:
        Takes a .mp4 and uses revspeech to try to convert it to a text file
        Prints status of upload and ID Number of  file uploaded
        Returns ID Number of instance
   ''' 
   #Defines comand used to transcribe 
   #TranscribeCMD = 'curl -X POST "https://api.rev.ai/revspeech/v1beta/jobs"'+' -H "Authorization: Bearer '+ Key + '"'+' -H "Content-Type: application/json" '+'-d "{\\"media_url\\"'+':\\"'+mp4URL+'\\"'+',\\"metadata\\"'+':\\"This is a sample submit jobs option\\"}" > tmp'
   TranscribeCMD = 'curl -X POST "https://api.rev.ai/revspeech/v1beta/jobs"'+' -H "Authorization: Bearer '+ Key + '"'+' -H "Content-Type: multipart/form-data" '+'-F "media=@C:\\Users\\dbli2\\Dropbox (MIT)\\HackMIT 2018\\'+FileName+';type=audio/mp3" '+'-F'+' "options={\\"metadata\\"'+':\\"This is a sample submit jobs option\\"}" > tmp'
   #print(TranscribeCMD)
   
   #Runs TranscribeCMD in command prompt
   os.system(TranscribeCMD)
   
   #Takes output from command  prompt and converts it to a dictionary
   TranscribeOutString = open('tmp', 'r').read()
   TranscribeOut= ast.literal_eval(TranscribeOutString)
   
   #Extracts information from TranscribeOut dictionary
   IDNumber = TranscribeOut["id"]
   Status = TranscribeOut["status"]
   
   #Prints information
   print("ID Number: " + IDNumber)
   print("Status: " +  Status)
   
   #Returns IDNumber
   return(IDNumber)
