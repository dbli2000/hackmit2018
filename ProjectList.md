# Possible Projects
List of possible projects for HackMIT 2018:

    Machine Learning on  EKG  data

    Basecalling to Indel analysis off of MinION
    
    Update and generalize SPAMALOT CRISPR Pipeline
    
    ScheduleBuilder for MIT
    
    Something something wolfram Education app
    
    Something about disease and epidemic mapping with  ML
    
    Speech to text -> NLP location mapping -> location  on  map

Documentation:
    At the beginning of each file please format like so:
    
    '''
        inputs: the file format the script takes as an argument
        
        outputs: the file format the script returns, what processing has been done
        
        summary: the means by which the process described above is implemented, theoretical underpinning
        
        misc:  any pitfalls or unintuitive parts of the code, plz be a good samaritan
    '''