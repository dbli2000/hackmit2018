# Tasks
List of tasks required:

    Set up automated phone call with prompt
    
    Capture phone call as a .mp4 file
    
    Use RevSpeech to convert .mp4 to a text file
    
    Extract location response from text file with a Regex
    
    Input text string into some sort of NLP software and extract locations/features of the location
    
    Input location/features of location into google maps/places
    
    Plot location on map