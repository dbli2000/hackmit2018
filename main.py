# -*- coding: utf-8 -*-
"""
Created on Sat Sep 15 23:59:47 2018

@author: dbli2
"""
#Import required modules
from RevSpeech import RevSpeech
from ExtractCoordinates import extractLocationString, getLocationCoordinates
from WriteHTML import writeHTML
import webbrowser, os

coords = []
names = []
needs = []

#  Define authetification keys, file names, and mp3 URLs as needed
key = "018F1t3lXHg_RiVmhK_omA8xeokgkAo28fj07Vsv_9fbxUhR0Hml10TqR2vf12jPItGZQxUlvIxvTyPX8SM8pGsC5WjDQ" #revspeech authorization key
#allmp4urls = ["https://dl.dropboxusercontent.com/s/hvka1lly8rchvek/Daniel%204.mp3", "https://dl.dropboxusercontent.com/s/z6vybcs2xifv4qd/Diana%20Aneurysm.mp3"]

filenames = os.listdir("C:\\Users\\dbli2\\Dropbox (MIT)\\HackMIT 2018")
filenames.remove('desktop.ini')
filenames.remove('.dropbox')
'''
filenames = ["AHwOX_Dyal51jsehBKQpBsX10wJv4y3uvWaz5cB_NGSF9rNiEuRtMDrCfhmTINOOcPVUEIfr1x_Oi6MQpZkX9PfX8ubKkXl2SXvA7wLXG2Qr3YOBhndyx7b0hwmWbdNirg1WNX_VWoDjKuXDePoNMnfW3vNeRmvEqQ.mp3"]
'''
for filename in filenames:
    #Recover string of text
    print(filename)
    transcript = RevSpeech(key,  filename)
    
    #Extract information from text
    location, name, need = extractLocationString(transcript)
    
    # Look for coordinates corresponding to the location string (if none found, passes)
    coord = getLocationCoordinates(location)
    if coord:
        coords.append(coord)
        names.append(name)
        needs.append(need)

#Writes the coords into points on google maps in HTML file Output.html 
writeHTML(coords, names, needs)

#Opens HTML file in browser
webbrowser.open('file://' + os.path.realpath("Output.html"))

'''
#Old Code for iterating through dropbox URLs
for mp4url in allmp4urls:
    #Recover string of text
    transcript = RevSpeech(key,  mp4url)
    
    #Extract information from text
    location, name, need = extractLocationString(transcript)
    
    # Look for coordinates corresponding to the location string (if none found, passes)
    coord = getLocationCoordinates(location)
    if coord:
        coords.append(coord)
        names.append(name)
        needs.append(need)
'''
