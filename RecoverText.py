# -*- coding: utf-8 -*-
"""
Created on Sat Sep 15 14:46:57 2018

@author: dbli2
"""
#Import  required modules
import os
import time

def RecoverText(Key, ID):
   '''
    Inputs: 
        Key: Authentication Key for RevSpeech
        ID: ID for .mp4 file to be transcribed
        
    Outputs: 
        Returns text of processed RevSpeech file
        
    Summary:
        Takes in the ID of a RevSpeech submission and returns text of the file
   ''' 
   #Defines comand used to recover transcript
   RecoverCMD ='curl -X GET "https://api.rev.ai/revspeech/v1beta/jobs/'+ ID + '/transcript"' + ' -H "Authorization: Bearer ' + Key + '"' +' -H "Accept: text/plain" > tmp'
   
   #Runs RecoverCMD in command prompt
   os.system(RecoverCMD)
   
   #Takes output from command  prompt (transcript) and opens it
   RecoverTextString = open('tmp', 'r').read()
   
   #If time has not been enough, waits till transcript is finished
   while RecoverTextString[0]  =='{':
       #Small time delay to allow processing of file
       time.sleep(10)
       os.system(RecoverCMD)
       RecoverTextString = open('tmp', 'r').read()
    
   #Prints information
   print(RecoverTextString)
   
   #Returns transcript string
   return(RecoverTextString)
